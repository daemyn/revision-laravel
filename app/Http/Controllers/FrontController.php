<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $title = 'Home';
        $posts = Post::with('category')->orderBy('id', 'DESC')->get();
        return view('front.index', compact('title', 'posts'));
    }

    public function category($id)
    {
        if(!$category = Category::with('posts')->find($id)) return redirect()->route('index');
        $title = $category->name;
        $posts = $category->posts;
        return view('front.category', compact('title', 'posts'));
    }
}
